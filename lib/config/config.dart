import 'package:flutter/material.dart';

enum Build { testing, production, staging, local }

class Config {
  static final navigatorKey = GlobalKey<NavigatorState>();
  static const Build server = Build.testing;
  static bool isProduction = server == Build.production;

  static String get baseUrl {
    switch (server) {
      case Build.testing:
        return "http://185.188.250.204:3001/";
      case Build.production:
        return "https://api.b-seenapp.com/";
      case Build.local:
        return "http://10.39.1.196:3000/";
      case Build.staging:
        return "https://bseenmvp.herokuapp.com/";
    }
  }

  static String? token;
}
